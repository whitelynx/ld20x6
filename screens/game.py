import logging
import os.path
from random import random

try:
    from collections import OrderedDict
except ImportError:
    from compat.ordereddict import OrderedDict

import pygame

from .base import BaseScreen

from sprites.playerShip import PlayerShip
from sprites.star import Star
from sprites.cloud import Cloud
from sprites.parallax import visibleForDepth
from sprites.enemyShip import EnemyShip

logger = logging.getLogger("screens.game")


class GameScreen(BaseScreen):
    # enemySpawnProbability = 0.01
    enemySpawnProbability = 0.1

    def __init__(self, app):
        super(GameScreen, self).__init__(app)

        pygame.mouse.set_visible(False)
        pygame.event.set_grab(True)

        self.allSprites = dict()
        self.levelProgress = 0
        self.lastLevelProgress = 0
        self.speed = 0.1
        self.levelWidth = pygame.display.get_surface().get_width()
        self.levelLength = pygame.display.get_surface().get_height() * 20
        self.maxLayer = 200
        self.layerOffset = 5
        self.paused = False

        self.onLoadFinished.append(self.startMusic)

        # Events
        self.messageEvent = app.newUserEvent()

    def startMusic(self):
        if pygame.mixer.music.get_busy():
            pygame.mixer.music.fadeout(100)

        pygame.mixer.music.load(os.path.join("sounds", "music", "descentsitized.ogg"))
        pygame.mixer.music.play(-1)

    def load(self):
        self.outstandingLoads = set([self])
        self.allSprites = OrderedDict(
            (
                ("stars", pygame.sprite.LayeredDirty()),
                ("scenery", pygame.sprite.LayeredDirty()),
                ("projectiles", pygame.sprite.LayeredDirty()),
                ("ships", pygame.sprite.LayeredDirty()),
                ("shields", pygame.sprite.LayeredDirty()),
                ("effects", pygame.sprite.LayeredDirty()),
                ("clouds", pygame.sprite.LayeredDirty()),
            )
        )

        yield None

        self.stars = list()

        for i in range(100):
            newStar = Star(self)
            newStar.randomizePosition()
            self.stars.append(newStar)
            self.trackLoading(newStar)
            newStar.startLoad()
            self.addSprite(newStar, "stars")

            yield None

        self.clouds = list()

        for i in range(10):
            newCloud = Cloud(self)
            newCloud.randomizePosition()
            self.clouds.append(newCloud)
            self.trackLoading(newCloud)
            newCloud.startLoad()
            self.addSprite(newCloud, "clouds")

            yield None

        self.ship = PlayerShip(self)
        self.trackLoading(self.ship)
        self.ship.startLoad()

        self.addSprite(self.ship, "ships")

        yield None

        self.objectFinishedLoading(self)
        self.checkLoadFinished()

    def cleanup(self):
        pygame.event.set_grab(False)

        # Clean up sprites.
        for spriteGroup in list(self.allSprites.values()):
            spriteGroup.empty()

        self.allSprites = None

    def checkLoadFinished(self):
        if len(self.outstandingLoads) == 0:
            self.loadFinished()

    def trackLoading(self, obj):
        def finished():
            self.objectFinishedLoading(obj)

        obj.onLoadFinished.append(finished)
        self.outstandingLoads.add(obj)

    def objectFinishedLoading(self, which):
        # logger.debug("Object %r finished loading.", which)
        self.outstandingLoads.remove(which)
        self.checkLoadFinished()

    def addSprite(self, sprite, category="projectiles"):
        self.allSprites[category].add(sprite)
        sprite.category = category

    def removeSprite(self, sprite, category="projectiles"):
        if category is None:
            logger.error("Removing sprite that's not in a category! %r", sprite)
            return

        self.allSprites[category].remove(sprite)
        sprite.category = None

    def changeSpriteLayer(self, sprite, layer, category="projectiles"):
        if sprite not in self.allSprites[category]:
            return
        try:
            self.allSprites[category].change_layer(sprite, layer)
        except Exception as e:
            logger.debug(
                "Couldn't switch sprite %r to layer %d; %s", sprite, layer, str(e)
            )

    def handleEvent(self, event):
        if event.type == pygame.KEYDOWN and event.key in (pygame.K_PAUSE, pygame.K_p):
            self.paused = not self.paused

        else:
            self.ship.handleEvent(event)

    def frame(self):
        if not self.paused:
            # Clear screen.
            self.app.screen.fill((0, 0, 0))

            # Render all sprites.
            for spriteGroup in list(self.allSprites.values()):
                spriteGroup.update()
                spriteGroup.draw(self.app.screen)

            # Draw the HUD.
            self.ship.drawHUD()

            self.lastLevelProgress = self.levelProgress
            self.levelProgress += self.app.clock.get_time() * self.speed

            # if random() < self.enemySpawnProbability:
            if not pygame.key.get_mods() & pygame.KMOD_LSHIFT:
                from datetime import datetime, timedelta

                if not hasattr(
                    self, "lastEnemySpawned"
                ) or datetime.now() - self.lastEnemySpawned > timedelta(seconds=2):
                    self.spawnEnemy()
                    self.lastEnemySpawned = datetime.now()

    def spawnEnemy(self):
        # TODO: Randomize which enemy is spawned.
        newEnemy = EnemyShip(self)
        self.trackLoading(newEnemy)
        newEnemy.startLoad()

        def finishLoading():
            newEnemy.randomizePosition(
                y=lambda z: self.viewportAtDepth(z).top
                - (newEnemy.image.get_height() / 2)
            )

            self.addSprite(newEnemy, "ships")

        newEnemy.onLoadFinished.append(finishLoading)

    def checkCollisions(self, sprite, ignore=lambda x: None):
        collisions = list()
        spriteGroup = pygame.sprite.GroupSingle(sprite)

        for group in map(self.allSprites.get, ("projectiles", "shields", "ships")):
            collisionsThisGroup = pygame.sprite.groupcollide(
                spriteGroup, group, False, False
            )
            if sprite in collisionsThisGroup:
                for target in collisionsThisGroup[sprite]:
                    # TODO: Decide if we should be using collide masks... if so,
                    # we need to modify them to take into account movement
                    # since last frame, which could be difficult.
                    # if not ignore(target) and \
                    #        pygame.sprite.collide_mask(sprite, target):
                    if (target.visible or target.collideWhenHidden) and not ignore(
                        target
                    ):
                        collisions.append(target)

        return collisions

    @property
    def viewCenter(self):
        return 0, -self.levelProgress

    def viewportAtDepth(self, z):
        visibleXAtLevel = pygame.display.get_surface().get_width() * visibleForDepth(
            self, z
        )
        visibleYAtLevel = pygame.display.get_surface().get_height() * visibleForDepth(
            self, z
        )
        viewport = pygame.Rect(0, 0, visibleXAtLevel, visibleYAtLevel)
        viewport.center = self.viewCenter
        return viewport
