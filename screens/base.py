# import logging
import weakref

# logger = logging.getLogger('screens.base')


class BaseScreen(object):
    def __init__(self, app):
        # Prevent circular references using a weakref.
        self.__app = weakref.ref(app)

        self.isLoadStarted = False
        self.isLoadFinished = False
        self.onLoadFinished = list()

    @property
    def app(self):
        return self.__app()

    def startLoad(self):
        self.isLoadStarted = True
        self.app.startLoading(self.load())

    def load(self):
        yield None

        self.loadFinished()

    def loadFinished(self):
        self.isLoadFinished = True

        for callback in self.onLoadFinished:
            callback()

        self.onLoadFinished = list()

    def cleanup(self):
        pass

    def handleEvent(self, event):
        pass

    def frame(self):
        pass
