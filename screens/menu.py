import logging
import json
import os.path

import pygame

from .base import BaseScreen
from .loading import LoadingScreen
from .game import GameScreen

logger = logging.getLogger("screens.menu")


class MenuScreen(BaseScreen):
    screens = {
        "GameScreen": GameScreen,
    }

    def __init__(self, app):
        super(MenuScreen, self).__init__(app)

        pygame.mouse.set_visible(1)

        self.pageCache = dict()

        self.setCurrentPage("introduction")

    def load(self):
        yield None

        self.titleFont = self.app.loadFont(48, "Kyrou 7 Wide Bold Xtnd.ttf")

        yield None

        self.promptFont = self.app.loadFont(8, "Kyrou 9 Regular Bold Xtnd.ttf")
        self.textFont = self.app.loadFont(8, "Kyrou 7 Wide.ttf")

        self.loadFinished()

    def setCurrentPage(self, *pageFilePath):
        pageFilename = os.path.join("pages", *pageFilePath)

        if pageFilename not in self.pageCache:
            logging.debug("Loading page %r.", pageFilename)
            pageFile = open(pageFilename + ".json", "r")
            self.pageCache[pageFilename] = json.load(pageFile)

            keys = dict()
            for keyName, action in self.pageCache[pageFilename]["keys"].items():
                keys[getattr(pygame, keyName)] = action
                print("Binding key", keyName, "to:", action)
            self.pageCache[pageFilename]["keys"] = keys

            pageFile.close()

        self.currentPage = pageFilename

    @property
    def page(self):
        return self.pageCache[self.currentPage]

    def handleEvent(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key in self.page["keys"]:
                methodName = self.page["keys"][event.key][0]
                args = self.page["keys"][event.key][1:]
                getattr(self, methodName)(*args)

    def switchToScreen(self, screenName):
        self.app.switchToScreen(
            LoadingScreen(self.app, self.screens[screenName](self.app))
        )

    def frame(self):
        self.app.screen.fill((0, 0, 0))

        if not self.isLoadFinished:
            return

        shadowColor = self.page.get("shadowColor", (16, 16, 16))

        # Draw the menu text.
        textSurface = self.app.renderText(
            self.page["title"],
            self.titleFont,
            color=self.page["titleColor"],
            shadowColor=shadowColor,
            shadowOffset=3,
        )
        textpos = textSurface.get_rect(centerx=self.app.screen.get_width() / 2, top=0)
        self.app.screen.blit(textSurface, textpos)

        defaultTextColor = self.page["textColor"]

        textLines = self.page["text"]
        if isinstance(textLines, str):
            textLines = textLines.splitlines()

        for idx, line in enumerate(textLines):
            textColor = defaultTextColor
            if not isinstance(line, str):
                textColor = tuple(line[:3])
                line = line[3]

            textSurface = self.app.renderText(
                line,
                self.textFont,
                color=textColor,
                shadowColor=shadowColor,
                shadowOffset=3,
            )
            textpos = textSurface.get_rect(
                centerx=self.app.screen.get_width() / 2,
                centery=self.app.screen.get_height() / 2 + 30,
            )
            textpos.top += 16 * (idx - len(textLines) / 2.0)
            self.app.screen.blit(textSurface, textpos)

        textSurface = self.app.renderText(
            self.page["prompt"],
            self.promptFont,
            color=self.page["promptColor"],
            shadowColor=shadowColor,
            shadowOffset=3,
        )
        textpos = textSurface.get_rect(
            centerx=self.app.screen.get_width() / 2, bottom=self.app.screen.get_height()
        )
        self.app.screen.blit(textSurface, textpos)
