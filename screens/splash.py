# import logging
import os.path
from datetime import datetime, timedelta

import pygame

from .base import BaseScreen
from .menu import MenuScreen

# logger = logging.getLogger('screens.splash')


class SplashScreen(BaseScreen):
    # splashScreenDelay = timedelta(milliseconds=5000)
    splashScreenDelay = timedelta(milliseconds=1000)

    def __init__(self, app):
        super(SplashScreen, self).__init__(app)

        pygame.mouse.set_visible(0)

        if pygame.mixer.music.get_busy():
            pygame.mixer.music.fadeout(100)

        pygame.mixer.music.load(os.path.join("sounds", "music", "initialisms.ogg"))
        pygame.mixer.music.play(-1)

        self.titleText = app.title
        self.subtitleText = app.subtitle
        self.authorText = "by " + app.author
        self.websiteText = app.website

        self.screenOpenedAt = datetime.now()

    def frame(self):
        self.app.screen.fill((0, 0, 0))

        # Draw the title text.
        textSurface = self.app.renderText(
            self.titleText,
            self.app.titleFont,
            color=(72, 72, 72),
            shadowColor=(16, 16, 16),
            shadowOffset=3,
        )
        textpos = textSurface.get_rect(
            centerx=self.app.screen.get_width() / 2,
            centery=self.app.screen.get_height() / 2,
        )
        textpos.top -= 15

        self.app.screen.blit(textSurface, textpos)

        # Draw the subtitle text.
        textSurface = self.app.renderText(
            self.subtitleText,
            self.app.largeFont,
            shadowColor=(32, 32, 32),
            shadowOffset=2,
        )
        textpos = textSurface.get_rect(
            centerx=self.app.screen.get_width() / 2,
            centery=self.app.screen.get_height() / 2,
        )
        textpos.top += 10

        self.app.screen.blit(textSurface, textpos)

        # Draw the website text.
        textSurface = self.app.renderText(
            self.websiteText,
            self.app.mediumFont,
            color=(0, 64, 255),
            shadowColor=(32, 32, 32),
            shadowOffset=1,
        )
        textpos = textSurface.get_rect(
            centerx=self.app.screen.get_width() / 2,
            centery=self.app.screen.get_height() * 0.75,
        )
        textpos.top += 5

        self.app.screen.blit(textSurface, textpos)

        # Draw the author text.
        textSurface = self.app.renderText(
            self.authorText,
            self.app.mediumFont,
            color=(128, 128, 128),
            shadowColor=(32, 32, 32),
            shadowOffset=1,
        )
        textpos = textSurface.get_rect(
            centerx=self.app.screen.get_width() / 2,
            centery=self.app.screen.get_height() * 0.75,
        )
        textpos.top -= 5

        self.app.screen.blit(textSurface, textpos)

        if datetime.now() - self.screenOpenedAt > self.splashScreenDelay:
            self.app.switchToScreen(MenuScreen(self.app))
