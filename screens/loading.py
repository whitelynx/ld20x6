import logging
from datetime import datetime, timedelta

import pygame

from .base import BaseScreen

logger = logging.getLogger("screens.loading")


class LoadingScreen(BaseScreen):
    def __init__(self, app, nextModule):
        super(LoadingScreen, self).__init__(app)

        pygame.mouse.set_visible(0)

        self.nextModule = nextModule
        nextModule.onLoadFinished.append(self.switchToNextModule)
        nextModule.startLoad()

        self.lastTextUpdate = datetime.now()

        self.loadingText = "Loading."

    def frame(self):
        self.app.screen.fill((0, 0, 0))

        # Update the "Loading..." text.
        if datetime.now() - self.lastTextUpdate > timedelta(milliseconds=500):
            if self.loadingText.count(".") < 5:
                self.loadingText += "."
            else:
                self.loadingText = "Loading."

            self.lastTextUpdate = datetime.now()

        # Draw the "Loading..." text.
        textSurface = self.app.renderText(
            self.loadingText,
            self.app.titleFont,
            shadowColor=(32, 32, 32),
            shadowOffset=3,
        )
        textpos = textSurface.get_rect(centery=self.app.screen.get_height() / 2)
        textpos.left = 10

        self.app.screen.blit(textSurface, textpos)

    def switchToNextModule(self):
        logger.debug("Next module finished loading; switching to it.")
        self.app.switchToScreen(self.nextModule)
