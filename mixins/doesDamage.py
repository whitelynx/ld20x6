import logging

logger = logging.getLogger("mixins.doesDamage")


class DoesDamage(object):
    def __init__(self, *args, **kwargs):
        super(DoesDamage, self).__init__(*args, **kwargs)

        self.maxHealth = kwargs.get("maxHealth", 100)
        self.damage = 0

        self.onDestroyed = list()
        self.ignoredSprites = list()

    @property
    def responsibleForDamage(self):
        if hasattr(self, "firedBy"):
            return self.firedBy
        else:
            return self

    def update(self):
        collisions = self.game.checkCollisions(self, ignore=self.checkIgnoredCollision)
        if len(collisions) > 0:
            for collided in collisions:
                if hasattr(collided, "doDamage"):
                    collided.doDamage(self.damage, self.responsibleForDamage)

                # TODO: Explosions!

        super(DoesDamage, self).update()

    def checkIgnoredCollision(self, other):
        # Ignore collisions with other if they're in our ignored sprites...
        if other in self.ignoredSprites:
            return True

        # If their parent is in our explicitly ignored sprites...
        if hasattr(other, "parent") and other.parent in self.ignoredSprites:
            return True

        # Or, if they're explicitly ignoring some of the same sprites as us.
        if (
            hasattr(other, "ignoredSprites")
            and len(set(other.ignoredSprites) & set(self.ignoredSprites)) > 0
        ):
            return True

        return False
