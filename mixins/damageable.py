import logging

logger = logging.getLogger("mixins.damageable")


class Damageable(object):
    maxHealth = 100

    def __init__(self, *args, **kwargs):
        super(Damageable, self).__init__(*args, **kwargs)

        self.damageReceived = 0

        self.onDestroyed = list()

    @property
    def health(self):
        return self.maxHealth - self.damageReceived

    def doDamage(self, damage, damageFrom):
        self.damageReceived += damage
        if self.damageReceived > self.maxHealth:
            self.destroyed(damageFrom)

            if hasattr(damageFrom, "destroyedOther"):
                damageFrom.destroyedOther(self)

            for callback in self.onDestroyed:
                callback(self)

            self.remove()

    def destroyed(self, destroyedBy):
        logger.info("%r destroyed by %r.", self, destroyedBy)
