#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Ludum Dare 20 contest entry

Copyright (c) 2011 David H. Bronke <whitelynx@gmail.com>
Licensed under the MIT license; see the LICENSE file for details.

"""
import os
import logging

import pygame

from screens.splash import SplashScreen

logger = logging.getLogger("ld20")


class config:
    desired_fps = 80


class App(object):
    title = "LD20X6"
    subtitle = "Protecting You From Danger!"
    author = "whitelynx of Skewed Aspect"
    website = "http://people.g33xnexus.com/whitelynx"

    def __init__(self):
        # Initialize Pygame
        pygame.init()
        pygame.font.init()
        self.screen = pygame.display.set_mode((800, 600))
        # pygame.OPENGL | pygame.DOUBLEBUF)
        # pygame.FULLSCREEN | pygame.HWSURFACE | pygame.DOUBLEBUF)
        pygame.display.set_caption("%s: %s" % (self.title, self.subtitle))
        self.clock = pygame.time.Clock()

        # Last user event ID we assigned.
        self.lastUserEventID = pygame.USEREVENT

        # Caches
        # TODO: Cache expiration?
        self.fontCache = dict()
        self.imageCache = dict()
        self.soundCache = dict()
        self.messagesCache = dict()

        # Loaders that are currently in need of loading time
        self.processingLoaders = list()

        # Fonts we'll be using
        self.largeFont = self.loadFont(8, "Kyrou 9 Regular Bold Xtnd.ttf")
        self.mediumFont = self.loadFont(8, "Kyrou 7 Wide.ttf")
        self.smallFont = self.loadFont(8, "Kyrou 5 Wide.ttf")
        self.titleFont = self.loadFont(48, "Kyrou 7 Wide Bold Xtnd.ttf")

        # The current screen
        self.currentGameScreen = None

        self.switchToScreen(SplashScreen(self))

    def switchToScreen(self, screen):
        logger.debug("Switching to screen: %r", screen)
        lastScreen = self.currentGameScreen

        self.currentGameScreen = screen

        if not screen.isLoadStarted and not screen.isLoadFinished:
            screen.startLoad()

        if lastScreen is not None:
            lastScreen.cleanup()

    def newUserEvent(self):
        self.lastUserEventID += 1
        logger.debug("Registered event with ID %r.", self.lastUserEventID)
        return self.lastUserEventID

    def loadFont(self, size, *fontFilePath):
        fontFilename = os.path.join("fonts", *fontFilePath)

        if fontFilename not in self.fontCache:
            logger.debug("Loading font %r.", fontFilename)
            self.fontCache[fontFilename] = pygame.font.Font(fontFilename, size)

        return self.fontCache[fontFilename]

    def loadImage(self, *imageFilePath, **kwargs):
        useAlpha = kwargs.get("alpha", False)
        imageFilename = os.path.join("images", *imageFilePath)

        if imageFilename not in self.imageCache:
            logger.debug("Loading image %r. (useAlpha=%r)", imageFilename, useAlpha)
            self.imageCache[imageFilename] = pygame.image.load(imageFilename)
            if useAlpha:
                self.imageCache[imageFilename] = self.imageCache[
                    imageFilename
                ].convert_alpha()
            else:
                self.imageCache[imageFilename] = self.imageCache[
                    imageFilename
                ].convert()

        return self.imageCache[imageFilename].copy()

    class NoneSound:
        def play(self):
            pass

    def loadSound(self, *soundFilePath):
        soundFilename = os.path.join("sounds", *soundFilePath)

        if soundFilename not in self.soundCache:
            logger.debug("Loading sound %r.", soundFilename)

            if not pygame.mixer:
                return self.NoneSound()

            try:
                if not os.path.exists(soundFilename):
                    raise pygame.error("File does not exist!")

                self.soundCache[soundFilename] = pygame.mixer.Sound(soundFilename)

            except pygame.error as message:
                logger.error("Cannot load sound %r: %s", soundFilename, message)
                self.soundCache[soundFilename] = self.NoneSound()

        return self.soundCache[soundFilename]

    def loadMessages(self, *filename):
        if len(filename) == 1 and not isinstance(filename[0], str):
            filename = filename[0]
        filename = ["messages"] + list(filename)
        filename = os.path.join(*filename)

        if filename not in self.messagesCache:
            messageFile = open(filename, "r")
            self.messagesCache[filename] = messageFile.readlines()
            messageFile.close()

        return self.messagesCache[filename]

    def cssTuple(self, value):
        # CSS-style tuples, like padding/margin
        if isinstance(value, (int, float)):
            return (value, value, value, value)

        elif len(value) == 1:
            return (value[0], value[0], value[0], value[0])

        elif len(value) == 2:
            return (value[0], value[1], value[0], value[1])

        elif len(value) == 3:
            return (value[0], value[1], value[2], value[1])

    def startLoading(self, loadable):
        if loadable is not None:
            self.processingLoaders.append(loadable)

    def renderText(self, text, font, **kwargs):
        # Optional arguments.
        color = kwargs.get("color", (255, 255, 255))
        background = kwargs.get("background", (0, 0, 0, 0))
        shadowColor = kwargs.get("shadowColor", None)
        shadowOffset = kwargs.get("shadowOffset", 1)
        antialias = kwargs.get("antialias", False)
        padding = self.cssTuple(kwargs.get("padding", 0))
        # TODO: Implement these!
        # width = kwargs.get('width', None)
        # height = kwargs.get('height', None)

        textSurface = font.render(text, antialias, color)

        if shadowColor:
            if isinstance(shadowOffset, int):
                shadowOffset = (shadowOffset, shadowOffset)

        finalTextSurf = pygame.Surface(
            (
                textSurface.get_width() + shadowOffset[1] + padding[1] + padding[3],
                textSurface.get_height() + shadowOffset[0] + padding[0] + padding[2],
            ),
            flags=pygame.SRCALPHA,
        )

        if background:
            finalTextSurf.fill(background)

        if shadowColor:
            shadowSurface = font.render(text, True, shadowColor)
            shadowOffset = (shadowOffset[0] + padding[3], shadowOffset[1] + padding[0])
            finalTextSurf.blit(shadowSurface, shadowOffset)

        finalTextSurf.blit(textSurface, (padding[3], padding[0]))

        return finalTextSurf

    def run(self):
        self.run = True

        while self.run:
            # Lock the framerate at some given FPS
            self.clock.tick(config.desired_fps)

            # Handle events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False
                    break
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    self.run = False
                    break
                else:
                    self.currentGameScreen.handleEvent(event)

            self.currentGameScreen.frame()
            pygame.display.flip()

            if len(self.processingLoaders) > 0:
                try:
                    next(self.processingLoaders[0])
                except StopIteration:
                    del self.processingLoaders[0]

        pygame.display.flip()
        pygame.quit()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    app = App()
    app.run()
