"""Setup script for LD20X6: Initialisms

"""
from distutils.core import setup
import glob

try:
    import py2exe

except ImportError:
    print("py2exe not found! You won't be able to generate installers.")

else:
    import os

    origIsSystemDLL = py2exe.build_exe.isSystemDLL

    def isSystemDLL(pathname):
        """Override built-in isSystemDLL() to explicitly allow SDL_ttf.dll and
        libogg-0.dll.

        """
        if os.path.basename(pathname).lower() in ("sdl_ttf.dll", "libogg-0.dll"):
            return 0

        return origIsSystemDLL(pathname)

    py2exe.build_exe.isSystemDLL = isSystemDLL


# TODO: Try converting the readme to ReST with pandoc?
readme = open("README.mkd", "r")
long_description = readme.read()
readme.close()


setup(
    name="LD20X6",
    version="0.1",
    description="LD20X6: Initialisms",
    long_description=long_description,
    author="David H. Bronke",
    author_email="whitelynx@gmail.com",
    url="http://projects.g33xnexus.com/ld20x6/wiki/README/",
    options={"py2exe": {"bundle_files": 1}},
    # TODO: Fix error logs and switch back to 'windows' instead of 'console'
    # windows = [{'script': "ld20x6.py"}],
    console=["ld20x6.py"],
    zipfile=None,
    packages=[
        "compat",
        "mixins",
        "screens",
        "sprites",
        "util",
    ],
    data_files=[
        (".", ["LICENSE", "README.mkd"]),
        ("fonts", glob.glob("fonts/*")),
        ("images/backgrounds", glob.glob("images/backgrounds/*")),
        ("images/ships", glob.glob("images/ships/*")),
        ("images/weapons", glob.glob("images/weapons/*")),
        ("messages", glob.glob("messages/*")),
        ("pages", glob.glob("pages/*")),
        ("sounds/misc", glob.glob("sounds/misc/*")),
        ("sounds/weapons", glob.glob("sounds/weapons/*")),
    ],
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Intended Audience :: End Users/Desktop",
        "Topic :: Games/Entertainment :: Arcade",
        "Programming Language :: Python :: 2",
        "Operating System :: OS Independent",
        "Development Status :: 2 - Pre-Alpha",
    ],
)
