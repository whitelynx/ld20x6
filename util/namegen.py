from random import random, randrange, choice

names = [
    "bob",
    "ed",
    "frank",
    "fred",
    "marta",
    "bud",
    "spud",
    "xavier",
    "winnifred",
    "manfred",
    "annwyn",
    "tom",
    "bill",
    "dick",
]


def generateName():
    rnd = random()

    if rnd < 0.5:
        return choice(names).capitalize()

    else:
        name = ""
        for i in range(randrange(3, 12)):
            if i % 2:
                name += choice("aeiouy")
            else:
                name += choice("bcdfghjklmnpqrstvwxz")
        return name.capitalize()
