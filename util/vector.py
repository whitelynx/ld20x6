from math import sqrt


def vectorLength(vec):
    return sqrt(sum([n**2 for n in vec]))


def unitVector(vec):
    length = vectorLength(vec)
    return tuple([x / length for x in vec])
