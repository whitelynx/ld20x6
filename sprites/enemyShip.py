import logging
from functools import partial
from random import random, choice

import pygame

from .parallax import ParallaxSprite
from .missile import ProjectileLauncher
from mixins.damageable import Damageable
from mixins.doesDamage import DoesDamage
from util.namegen import generateName

logger = logging.getLogger("sprites.enemyShip")


class EnemyShip(Damageable, DoesDamage, ParallaxSprite):
    """An enemy ship."""

    weaponSlots = [
        {
            "projectileType": "BlueEnemyMissile",
            "hardpoints": [
                {
                    "offset": (-20, -40),
                    "direction": (-0.5, 10),
                    "variance": (2, 0),
                },
                {
                    "offset": (20, -40),
                    "direction": (0.5, 10),
                    "variance": (2, 0),
                },
            ],
        },
        {
            "projectileType": "BigRedEnemyMissile",
            "hardpoints": [
                {
                    "offset": (0, -50),
                    "direction": (0, 10),
                    "variance": (1, 0),
                },
            ],
        },
    ]

    deathMessageFilename = "enemyDeath"

    # howTalkative = 0.1
    howTalkative = 0.5

    damage = 40

    def __init__(self, game, speed=(0, 0.1)):
        super(EnemyShip, self).__init__(game, position=(0, 0, 0))

        self.speed = speed

        self.name = generateName()

    def __str__(self):
        return self.name

    def load(self):
        yield None

        self.loadImage("ships", "enemy.png", alpha=True)

        yield None

        self.deathMessages = self.game.app.loadMessages(self.deathMessageFilename)

        yield None

        for slot in self.weaponSlots:
            slot["weapon"] = ProjectileLauncher(
                self.game, self, slot["projectileType"], slot["hardpoints"]
            )
            self.game.trackLoading(slot["weapon"])

            def weaponFinishedLoading(weapon):
                if self.isLoadFinished:
                    weapon.startFire()
                else:
                    self.onLoadFinished.append(weapon.startFire)

            slot["weapon"].onLoadFinished.append(
                partial(weaponFinishedLoading, slot["weapon"])
            )
            slot["weapon"].startLoad()

            yield None

        self.loadFinished()

    def randomizePosition(self, **kwargs):
        if "z" in kwargs:
            z = kwargs["z"]
        else:
            z = 0

        viewport = self.game.viewportAtDepth(z)

        if "x" in kwargs:
            if callable(kwargs["x"]):
                x = kwargs["x"](z)
            else:
                x = kwargs["x"]
        else:
            x = random() * viewport.width + viewport.left

        if "y" in kwargs:
            if callable(kwargs["y"]):
                y = kwargs["y"](z)
            else:
                y = kwargs["y"]
        else:
            y = random() * viewport.height + viewport.top

        if self.imageRotation is not None:
            self.imageRotation = random() * 360

        self.position = (x, y, z)

    def update(self):
        viewport = self.game.viewportAtDepth(self.z)
        viewport.inflate_ip(80, 300)

        if not viewport.contains(pygame.Rect(self.x, self.y, 1, 1)):
            self.remove()

        elapsedTime = self.game.app.clock.get_time()
        self.x += self.speed[0] * elapsedTime
        self.y += self.speed[1] * elapsedTime

        super(EnemyShip, self).update()

    def destroyed(self, destroyedBy):
        logger.info("%r destroyed by %r.", self, destroyedBy)

        self.remove()

        # TODO: Spawn explosion!

        if random() < self.howTalkative:
            pygame.event.post(
                pygame.event.Event(
                    self.game.messageEvent,
                    sender=self,
                    message=choice(self.deathMessages),
                )
            )
