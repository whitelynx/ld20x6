import logging
from random import random

from .background import BackgroundSprite

logger = logging.getLogger("sprites.cloud")


class Cloud(BackgroundSprite):
    """A cloud."""

    minLayer = -0.1
    maxLayer = -4

    imageFilename = ("backgrounds", "cloud.png")

    def __init__(self, game, position=(0, 0, 0)):
        super(Cloud, self).__init__(game, position)

        self.imageRotation = random() * 360
