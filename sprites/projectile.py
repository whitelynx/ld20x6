import logging

import pygame

from .parallax import ParallaxSprite
from util.vector import unitVector
from mixins.damageable import Damageable
from mixins.doesDamage import DoesDamage

logger = logging.getLogger("sprites.projectile")


class Projectile(Damageable, DoesDamage, ParallaxSprite):
    """A projectile.

    Projectile definitions take the following form:

    "BigRedProjectile": {
        "imageFile": "missile.png",
        "fireSoundFile": "fire_missile.wav",
        "accel": 0.005,
        "initialSpeed": 20,
        "damage": 100,
        }

    """

    def __init__(
        self,
        game,
        definition="missile",
        position=(0, 0, 0),
        direction=(0, 10),
        firedBy=None,
        ignoredSprites=(),
        maxSpeed=3.2,
    ):
        super(Projectile, self).__init__(game, position)

        self.definition = definition
        self.ignoredSprites = ignoredSprites
        self.firedBy = firedBy

        self.damage = definition["damage"]

        self.direction = unitVector(direction)
        self.speed = definition["initialSpeed"]
        self.firstFrame = True

        self.maxSpeed = maxSpeed

        # Once we've loaded, play the firing sound.
        self.onLoadFinished.append(self.playFireSound)

        # Add self to the game's sprite list.
        self.game.addSprite(self, "projectiles")

    def playFireSound(self):
        self.definition["fireSound"].play()

    @classmethod
    def preload(cls, app):
        for variantName in cls.variants:
            yield None

            cls.variants[variantName]["image"] = app.loadImage(
                "weapons", cls.variants[variantName]["imageFile"], alpha=True
            )

            yield None

            cls.variants[variantName]["fireSound"] = app.loadSound(
                "weapons", cls.variants[variantName]["fireSoundFile"]
            )

            yield None

            cls.variants[variantName]["explosionSound"] = app.loadSound(
                "weapons", cls.variants[variantName]["explosionSoundFile"]
            )

        cls.preloadFinished()

    def load(self):
        self.image = self.definition["image"]

        self.loadFinished()

    def checkIgnoredCollision(self, other):
        if other in self.ignoredSprites:
            return True

        if hasattr(other, "parent") and other.parent in self.ignoredSprites:
            return True

        if (
            hasattr(other, "ignoredSprites")
            and len(set(other.ignoredSprites) & set(self.ignoredSprites)) > 0
        ):
            return True

        return False

    def update(self):
        if self.visible:
            if not self.firstFrame:
                visible = self.game.viewportAtDepth(self.z)
                visible.inflate_ip(40, 200)

                if not visible.contains(pygame.Rect(self.x, self.y, 1, 1)):
                    self.remove()

                elapsedTime = self.game.app.clock.get_time()

                self.speed += elapsedTime * self.definition["accel"]
                if self.speed > self.maxSpeed:
                    self.speed = self.maxSpeed

                self.x += self.direction[0] * elapsedTime * self.speed
                self.y += self.direction[1] * elapsedTime * self.speed

            else:
                self.firstFrame = False

        super(Projectile, self).update()

    def destroyed(self, destroyedBy):
        logger.info("%r destroyed by %r.", self, destroyedBy)

        self.remove()

        self.definition["explosionSound"].play()

        # TODO: Spawn explosion!
