import logging
import weakref

import pygame

logger = logging.getLogger("sprites.base")


class BaseSprite(pygame.sprite.DirtySprite):
    isPreloadStarted = False
    isPreloadFinished = False
    onPreloadFinished = list()

    showOnLoadFinished = True

    collideWhenHidden = False

    def __init__(self, game):
        pygame.sprite.DirtySprite.__init__(self)  # call Sprite intializer

        # Prevent circular references using a weakref.
        self.__game = weakref.ref(game)

        # Initially, we have no image.
        self.__image = None

        # Pretty much everything moves every frame, so there's no reason to
        # have to mark every sprite as dirty every frame. We're only using
        # DirtySprite so that we get the blending support for stars and clouds.
        self.dirty = 2

        # Leave invisible at least until load is finished.
        self.visible = False

        self.area = self.game.app.screen.get_rect()
        self.__rect = pygame.Rect(0, 0, 0, 0)

        self.isLoadStarted = False
        self.isLoadFinished = False
        self.onLoadFinished = list()

        self.onRemoved = list()

        self.category = None

    def get_rect(self):
        return self.__rect

    def set_rect(self, value):
        if value.top == 0 and value.left == 0:
            print(
                "==========> Rect at the origin! This should NOT happen! "
                + "(most of the time)"
            )
            import traceback

            traceback.print_stack()
        self.__rect = value

    rect = property(get_rect, set_rect)

    @property
    def game(self):
        return self.__game()

    @classmethod
    def startPreload(cls, app):
        app.startLoading(cls.preload(app))

    @classmethod
    def preload(cls, app):
        yield None

        cls.preloadFinished()

    @classmethod
    def preloadFinished(cls):
        cls.isPreloadFinished = True

        for callback in cls.onPreloadFinished:
            callback()

        cls.onPreloadFinished = list()

    def startLoad(self):
        self.isLoadStarted = True
        self.game.app.startLoading(self.load())

    def load(self):
        yield None

        self.loadFinished()

    def loadFinished(self):
        self.isLoadFinished = True

        for callback in self.onLoadFinished:
            callback()

        self.onLoadFinished = list()

        if self.showOnLoadFinished:
            self.visible = True

    def remove(self):
        self.game.removeSprite(self, self.category)

        for callback in self.onRemoved:
            callback()

    # Properties #

    def get_image(self):
        return self.__image

    def set_image(self, value):
        self.__image = value

        # TODO: If we use masks for collision, ENABLE THIS!
        # self.mask = pygame.mask.from_surface(self.image)

    image = property(get_image, set_image)
