import logging
import weakref
from random import random
from itertools import cycle
from datetime import datetime, timedelta

import pygame

from .parallax import ParallaxSprite
from .projectile import Projectile

logger = logging.getLogger("sprites.missile")


class ProjectileLauncher(ParallaxSprite):
    """A missile launcher."""

    projectileTypes = {
        "BigRedMissile": {
            "imageFile": "BigRedMissile.png",
            "fireSoundFile": "fire_BigRedMissile.wav",
            "explosionSoundFile": "explosion.wav",
            "accel": 0.003,
            "initialSpeed": 0,
            "damage": 100,
            "fireDelay": 900,
        },
        "BigRedEnemyMissile": {
            "imageFile": "BigRedMissile.png",
            "flipImageY": True,
            "fireSoundFile": "fire_BigRedMissile.wav",
            "explosionSoundFile": "explosion.wav",
            "accel": 0.003,
            "initialSpeed": 0,
            "damage": 50,
            "fireDelay": 1200,
        },
        "BlueMissile": {
            "imageFile": "BlueMissile.png",
            "fireSoundFile": "fire_missile.wav",
            "explosionSoundFile": "explosion.wav",
            "accel": 0.005,
            "initialSpeed": 0,
            "damage": 20,
            "fireDelay": 150,
        },
        "BlueEnemyMissile": {
            "imageFile": "BlueMissile.png",
            "flipImageY": True,
            "fireSoundFile": "fire_missile.wav",
            "explosionSoundFile": "explosion.wav",
            "accel": 0.005,
            "initialSpeed": 0,
            "damage": 10,
            "fireDelay": 200,
        },
        "Plasma": {
            "imageFile": "plasma.png",
            "fireSoundFile": "fire_plasma.wav",
            "explosionSoundFile": "plasma_hit.wav",
            "accel": 0,
            "initialSpeed": 0.7,
            "damage": 15,
            "fireDelay": 50,
        },
        "EnemyPlasma": {
            "imageFile": "plasma.png",
            "flipImageY": True,
            "fireSoundFile": "fire_plasma.wav",
            "explosionSoundFile": "plasma_hit.wav",
            "accel": 0,
            "initialSpeed": 0.7,
            "damage": 7,
            "fireDelay": 75,
        },
    }

    showOnLoadFinished = False

    def __init__(self, game, parent, projectileType="BlueMissile", hardpoints=[dict()]):
        super(ProjectileLauncher, self).__init__(game)

        # Prevent circular references using a weakref.
        self.__parent = weakref.ref(parent)

        self.parent.onRemoved.append(self.parentRemoved)

        self.projectileType = self.projectileTypes[projectileType]

        # Hardpoints
        self.hardpointList = hardpoints
        self.hardpointCount = len(hardpoints)
        self.hardpoints = cycle(hardpoints)

        # Track firing state
        self.lastFire = datetime.now() - timedelta(0, -self.projectileType["fireDelay"])
        self.isFiring = False

        # Which side do we fire the next missile from?
        self.nextMissileLeft = False

        # Add self to the game's sprite list.
        self.game.addSprite(self, "projectiles")

    @property
    def parent(self):
        return self.__parent()

    def parentRemoved(self):
        logger.debug(
            "Our parent sprite was removed... " + "We should probably stop shooting."
        )

        self.stopFire()

    def projectileFactory(self, *args, **kwargs):
        return Projectile(
            self.game,
            definition=self.projectileType,
            firedBy=self.parent,
            ignoredSprites=(self.parent,),
            *args,
            **kwargs
        )

    def load(self):
        yield None

        missileImage = self.game.app.loadImage(
            "weapons", self.projectileType["imageFile"], alpha=True
        )

        if self.projectileType.get("flipImageX", False) or self.projectileType.get(
            "flipImageY", False
        ):

            self.projectileType["image"] = pygame.transform.flip(
                missileImage,
                self.projectileType.get("flipImageX", False),
                self.projectileType.get("flipImageY", False),
            )

        else:
            self.projectileType["image"] = missileImage

        yield None

        self.projectileType["fireSound"] = self.game.app.loadSound(
            "weapons", self.projectileType["fireSoundFile"]
        )

        yield None

        self.projectileType["explosionSound"] = self.game.app.loadSound(
            "weapons", self.projectileType["explosionSoundFile"]
        )

        self.loadFinished()

    def startFire(self):
        if not self.isLoadFinished:
            logger.error(
                "Someone told us to start firing before we're "
                + "finished loaded! PANIC!!!"
            )
            return

        self.doFire()
        self.isFiring = True

    def stopFire(self):
        self.isFiring = False

    def doFire(self):
        # Calculating direction:
        hardpoint = next(self.hardpoints)
        variance = hardpoint.get("variance", (1, 1))
        direction = hardpoint.get("direction", (0, -10))
        offset = hardpoint.get("offset", (0, 0))
        direction = (
            random() * variance[0] - variance[0] / 2 + direction[0],
            random() * variance[1] - variance[1] / 2 + direction[1],
        )

        # FIXME: Why does this not work correctly?
        # scale = self.visibleForDepth(self.parent.z) \
        #        / self.visibleForDepth(self.parent.z + 1)
        # scale = self.scaleForDepth(self.parent.z + 1) \
        #        / self.scaleForDepth(self.parent.z)
        # pos = (scale * (self.parent.x + offset[0]),
        #        scale * (self.parent.y + offset[1]),
        #        scale * (self.parent.z + 1))

        pos = (self.parent.x + offset[0], self.parent.y + offset[1], self.parent.z)

        try:
            projectile = self.projectileFactory(position=pos, direction=direction)
        except Exception as e:
            logger.error("Couldn't create projectile: %s", str(e))
            return

        self.game.trackLoading(projectile)
        projectile.startLoad()

        self.lastFire = datetime.now()

    def update(self):
        # We shouldn't need to check for this, especially self.parent.visible.
        if self.parent is not None and self.parent.visible:
            self.position = self.parent.position

            if self.isFiring and (
                datetime.now() - self.lastFire
                > timedelta(
                    milliseconds=self.projectileType["fireDelay"] / self.hardpointCount
                )
            ):
                self.doFire()

            super(ProjectileLauncher, self).update()

    def setVisible(self, value):
        self.visible = value
