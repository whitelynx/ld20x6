import logging
from math import ceil

import pygame

from .base import BaseSprite

logger = logging.getLogger("sprites.parallax")


def toScreenCoords(game, *pos):
    if len(pos) == 1:
        pos = pos[0]

    x, y, z = pos
    scale = scaleForDepth(game, z)

    return (
        scale * (x - game.viewCenter[0]) + pygame.display.get_surface().get_width() / 2,
        scale * (y - game.viewCenter[1])
        + pygame.display.get_surface().get_height() / 2,
    )


def fromScreenCoords(game, *pos):
    # FIXME: Implement!
    pass


def scaleForDepth(game, z):
    return game.layerOffset / float(z + game.layerOffset)


def visibleForDepth(game, z):
    return float(z + game.layerOffset) / game.layerOffset


class ParallaxSprite(BaseSprite):
    """A parallax-affected sprite.

    All sprites have 3 coordinates. The third (z) represents the "depth" into
    the scene. (positive values are "lower")

    """

    def __init__(self, game, position=(0, 0, 0)):
        self.__z = None
        self.firstTimeSettingZ = True

        super(ParallaxSprite, self).__init__(game)

        self.position = position
        self.imageScale = 1
        self.imageRotation = None

        self.baseImage = None
        self.image = None

    def startLoad(self):
        self.isLoadStarted = True
        self.game.app.startLoading(self.load())

    def rescaleImage(self):
        if self.baseImage is not None:
            scale = self.scaleForDepth(self.depth) * self.imageScale
            if self.imageRotation is not None:
                self.image = pygame.transform.smoothscale(
                    self.baseImage,
                    (
                        int(ceil(self.baseImage.get_width() * 2)),
                        int(ceil(self.baseImage.get_height() * 2)),
                    ),
                )
                self.image = pygame.transform.rotate(self.image, self.imageRotation)
                self.image = pygame.transform.smoothscale(
                    self.image,
                    (
                        int(ceil(self.baseImage.get_width() * scale)),
                        int(ceil(self.baseImage.get_height() * scale)),
                    ),
                )
            else:
                self.image = pygame.transform.smoothscale(
                    self.baseImage,
                    (
                        int(ceil(self.baseImage.get_width() * scale)),
                        int(ceil(self.baseImage.get_height() * scale)),
                    ),
                )

            self.updateRectFromPosition()
            self.mask = pygame.mask.from_surface(self.image)

    def loadImage(self, *path, **kwargs):
        self.baseImage = self.game.app.loadImage(*path, **kwargs)
        self.rescaleImage()

    def toScreenCoords(self, *pos):
        return toScreenCoords(self.game, *pos)

    def fromScreenCoords(self, *pos):
        return fromScreenCoords(self.game, *pos)

    def scaleForDepth(self, z):
        return scaleForDepth(self.game, z)

    def visibleForDepth(self, z):
        return visibleForDepth(self.game, z)

    def update(self):
        # TODO: Why is this needed if we're already calling
        # updateRectFromPosition() every time we change the position?
        self.updateRectFromPosition()
        super(ParallaxSprite, self).update()

    # Properties #

    def updateRectFromPosition(self):
        if self.image is not None:
            rect = self.image.get_rect()
            rect.center = self.toScreenCoords(self.position)
            self.rect = rect
        # else:
        #    logger.warn("No image set on ParallaxSprite! (id: %s, hasattr: %s)" % (id(self), hasattr(self, 'image'), ))
        #    rect = pygame.Rect(0, 0, 1, 1)
        #    rect.center = self.toScreenCoords(self.position)
        #    self.rect = rect

    def get_x(self):
        return self.__x

    def set_x(self, value):
        if value == self.__x:
            # Do nothing; new value isn't different.
            return

        self.__x = value
        self.updateRectFromPosition()

    x = property(get_x, set_x)

    def get_y(self):
        return self.__y

    def set_y(self, value):
        if value == self.__y:
            # Do nothing; new value isn't different.
            return

        self.__y = value
        self.updateRectFromPosition()

    y = property(get_y, set_y)

    def get_z(self):
        return self.__z

    def set_z(self, value):
        if value == self.__z:
            # Do nothing; new value isn't different.
            return

        self.__z = value

        if not self.firstTimeSettingZ:
            self.game.changeSpriteLayer(self, self.layer)
        self.firstTimeSettingZ = False

        self.updateRectFromPosition()

    z = property(get_z, set_z)

    def get_position(self):
        return (self.x, self.y, self.z)

    def set_position(self, value):
        self.__x, self.__y, self.z = value

    position = property(get_position, set_position)

    @property
    def depth(self):
        return self.z

    def get_layer(self):
        """The layer this sprite is in.

        NOTE: This should _ONLY_ be set by the sprite group!

        """
        return self.game.maxLayer - self.z + self.game.layerOffset

    def set_layer(self, value):
        newZ = self.game.maxLayer - value + self.game.layerOffset
        if newZ != self.__z:
            logger.debug(
                "Got different Z from layer than we currently have! "
                + "(layer=%d, z=%d, newZ=%d)",
                value,
                self.__z,
                newZ,
            )
        self.__z = newZ

    layer = property(get_layer, set_layer)

    # Special Methods #

    def __repr__(self):
        return "<%s ParallaxSprite position=%s>" % (
            self.__class__.__name__,
            self.position,
        )
