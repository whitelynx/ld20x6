import logging
from random import random

import pygame

from .parallax import ParallaxSprite

logger = logging.getLogger("sprites.background")


class BackgroundSprite(ParallaxSprite):
    """A background / scenery sprite."""

    minLayer = 1
    maxLayer = 2

    imageFilename = ("backgrounds", "star.png")

    def __init__(self, game, position=(0, 0, 0)):
        super(BackgroundSprite, self).__init__(game, position)

        self.blendmode = pygame.BLEND_ADD

        self.imageScale = self.visibleForDepth(self.minLayer)

    def randomizePosition(self, **kwargs):
        if "z" in kwargs:
            z = kwargs["z"]
        else:
            # Ensure propert random distribution so we don't end up with
            # ridiculous amounts of far-distant stars and no close ones.
            minInverse = 1.0 / self.maxLayer
            maxInverse = 1.0 / self.minLayer
            z = int(1 / (random() * (maxInverse - minInverse) + minInverse))

        viewport = self.game.viewportAtDepth(z)

        if "x" in kwargs:
            if callable(kwargs["x"]):
                x = kwargs["x"](z)
            else:
                x = kwargs["x"]
        else:
            x = random() * viewport.width + viewport.left

        if "y" in kwargs:
            if callable(kwargs["y"]):
                y = kwargs["y"](z)
            else:
                y = kwargs["y"]
        else:
            y = random() * viewport.height + viewport.top

        if self.imageRotation is not None:
            self.imageRotation = random() * 360

        self.position = (x, y, z)
        self.rescaleImage()

    def load(self):
        yield None

        self.loadImage(*self.imageFilename)

        self.loadFinished()

    def update(self):
        viewport = self.game.viewportAtDepth(self.z)
        viewport.inflate_ip(40, 200)

        if not viewport.contains(pygame.Rect(self.x, self.y, 1, 1)):
            self.randomizePosition(
                y=lambda z: self.game.viewportAtDepth(z).top
                - (self.image.get_height() / 2)
            )

        super(BackgroundSprite, self).update()
