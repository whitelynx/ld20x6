import logging
import weakref
from random import randrange

import pygame

from .parallax import ParallaxSprite
from mixins.damageable import Damageable

logger = logging.getLogger("sprites.shield")


class Shield(Damageable, ParallaxSprite):
    """A shield."""

    showOnLoadFinished = False

    def __init__(self, game, parent):
        self.shimmerEvent = game.app.newUserEvent()

        super(Shield, self).__init__(game)

        # Prevent circular references using a weakref.
        self.__parent = weakref.ref(parent)

        # self.blendmode = pygame.BLEND_ADD

        self.maxHealth = 10000

        # Add self to the game's sprite list.
        self.game.addSprite(self, "shields")

    @property
    def parent(self):
        return self.__parent()

    def load(self):
        yield None

        self.loadImage("weapons", "shield.png")
        self.image.set_alpha(32, pygame.RLEACCEL)

        yield None

        self.shieldSound = self.game.app.loadSound("weapons", "shield.wav")

        self.shieldsDownSound = self.game.app.loadSound("weapons", "shields_down.wav")

        self.loadFinished()

    def handleEvent(self, event):
        if event.type == self.shimmerEvent:
            self.image.set_alpha(randrange(8, 96), pygame.RLEACCEL)

    def update(self):
        self.position = self.parent.position

        super(Shield, self).update()

    def setVisible(self, value):
        if self.health <= 0:
            # We can't bring the shields up if they're dead!
            value = False

        self.visible = value
        if value:
            pygame.time.set_timer(self.shimmerEvent, 50)
            self.shieldSound.play(-1)
        else:
            pygame.time.set_timer(self.shimmerEvent, 0)
            self.shieldSound.stop()

    def destroyed(self, destroyedBy):
        logger.info("Shields brought down by %r.", destroyedBy)

        self.setVisible(False)
        self.damageReceived = self.maxHealth

        self.shieldsDownSound.play()

    def destroyedOther(self, destroyed):
        self.parent.destroyedOther(destroyed)
