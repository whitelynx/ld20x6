import logging
from datetime import datetime, timedelta

import pygame

from .parallax import ParallaxSprite
from .shield import Shield
from .missile import ProjectileLauncher
from mixins.damageable import Damageable
from mixins.doesDamage import DoesDamage

logger = logging.getLogger("sprites.playerShip")


class PlayerShip(Damageable, DoesDamage, ParallaxSprite):
    """The player's ship."""

    weaponSlots = [
        {
            "projectileType": "Plasma",
            "hardpoints": [
                {
                    "offset": (-20, 0),
                    "direction": (-0.25, -10),
                    "variance": (0.5, 0),
                },
                {
                    "offset": (20, 0),
                    "direction": (0.25, -10),
                    "variance": (0.5, 0),
                },
            ],
        },
        {
            #'projectileType': 'BigRedMissile',
            "projectileType": "BlueMissile",
            "hardpoints": [
                {
                    "offset": (0, -10),
                    "direction": (0, -10),
                    "variance": (0.25, 0),
                },
            ],
        },
    ]

    maxMessageAge = timedelta(seconds=7)
    maxMessages = 5
    hudFormat = (
        "Health: %(health)d%%    Shield: %(shield)d%%    Ammo: "
        + "LOLWUT?    Kills/Deaths: %(kills)d/%(deaths)d"
    )

    damage = 40

    def __init__(self, game, **kwargs):
        super(PlayerShip, self).__init__(game, position=(0, 0, 0), **kwargs)

        self.maxHealth = 10000
        self.deathCount = 0
        self.killCount = 0
        self.messages = list()

    def load(self):
        yield None

        self.loadImage("ships", "player.png", alpha=True)

        yield None

        self.shield = Shield(self.game, self)
        self.game.trackLoading(self.shield)
        self.shield.startLoad()

        yield None

        for slot in self.weaponSlots:
            slot["weapon"] = ProjectileLauncher(
                self.game, self, slot["projectileType"], slot["hardpoints"]
            )
            self.game.trackLoading(slot["weapon"])
            slot["weapon"].startLoad()

            yield None

        self.loadFinished()

    def handleEvent(self, event):
        if self.visible:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    self.weaponSlots[0]["weapon"].startFire()

                elif event.button == 2:
                    self.activateShield()

                elif event.button == 3:
                    self.weaponSlots[1]["weapon"].startFire()

            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    self.weaponSlots[0]["weapon"].stopFire()

                elif event.button == 2:
                    self.deactivateShield()

                elif event.button == 3:
                    self.weaponSlots[1]["weapon"].stopFire()

        if event.type == self.game.messageEvent:
            logger.info("%r said: %r", event.sender, event.message)
            # TODO: Play message beep noise.
            self.messages.append((event.sender, event.message, datetime.now()))
            pass

        else:
            self.shield.handleEvent(event)

    def activateShield(self):
        self.shield.setVisible(True)

    def deactivateShield(self):
        self.shield.setVisible(False)

    def update(self):
        mousePos = pygame.mouse.get_rel()

        newProgress = self.game.levelProgress - self.game.lastLevelProgress
        x = self.position[0] + mousePos[0] / self.scaleForDepth(self.depth)
        y = (
            self.position[1]
            + mousePos[1] / self.scaleForDepth(self.depth)
            - newProgress
        )
        z = self.position[2]

        # Clip to viewport.
        rect = pygame.Rect(x, y, 1, 1)
        viewport = self.game.viewportAtDepth(z)
        rect.clamp_ip(viewport)

        x = rect.left
        y = rect.top

        self.position = (x, y, z)

        super(PlayerShip, self).update()

    def drawHUD(self):
        hudText = self.hudFormat % {
            "health": self.health,
            "shield": self.shield.health,  # TODO: Get health from shields!
            "kills": self.killCount,
            "deaths": self.deathCount,
        }

        textSurface = self.game.app.renderText(
            hudText,
            self.game.app.largeFont,
            background=(32, 32, 32, 192),
            shadowColor=(0, 0, 0),
            shadowOffset=2,
            padding=(2, 10, 0),
        )

        textpos = textSurface.get_rect(centerx=self.game.app.screen.get_width() / 2)
        textpos.top = 1

        self.game.app.screen.blit(textSurface, textpos)

        if len(self.messages) > self.maxMessages:
            self.messages = self.messages[-self.maxMessages :]

        messagesSurface = pygame.Surface(
            (self.game.app.screen.get_width() * 2 / 3, len(self.messages) * 14),
            flags=pygame.SRCALPHA,
        )
        messagesSurface.fill((16, 16, 16, 128))

        freshMessages = list()
        for idx, (sender, message, receivedAt) in enumerate(self.messages):

            textSurface = self.game.app.renderText(
                "%s: %s" % (sender, message),
                self.game.app.mediumFont,
                shadowColor=(0, 0, 0),
                shadowOffset=1,
            )
            textpos = textSurface.get_rect(
                centerx=self.game.app.screen.get_width() / 3, top=14 * idx
            )

            messagesSurface.blit(textSurface, textpos, special_flags=pygame.BLEND_ADD)

            if datetime.now() - receivedAt <= self.maxMessageAge:
                freshMessages.append((sender, message, receivedAt))

        textpos = messagesSurface.get_rect(
            centerx=self.game.app.screen.get_width() / 2,
            bottom=self.game.app.screen.get_height(),
        )

        self.game.app.screen.blit(messagesSurface, textpos)

        self.messages = freshMessages

    def destroyed(self, destroyedBy):
        logger.info("%r destroyed by %r.", self, destroyedBy)

        self.visible = False
        self.damageReceived = self.maxHealth

        # TODO: Spawn explosion!

        self.deathCount += 1

    def destroyedOther(self, destroyed):
        self.killCount += 1
