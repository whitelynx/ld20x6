import logging

from .background import BackgroundSprite

logger = logging.getLogger("sprites.star")


class Star(BackgroundSprite):
    """A star."""

    minLayer = 10
    maxLayer = 200

    imageFilename = ("backgrounds", "star.png")

    def __init__(self, game, position=(0, 0, 0)):
        super(Star, self).__init__(game, position)

        # self.imageRotation = random() * 360
