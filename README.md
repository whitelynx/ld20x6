LD20X6: Initialisms
===================
[A Ludum Dare 20 jam entry][]  
by David H. Bronke (<whitelynx@gmail.com>)

[A Ludum Dare 20 jam entry]: https://web.archive.org/web/20191103182132/http://ludumdare.com:80/compo/ludum-dare-20/?action=preview&uid=4408

![LD20X6 screenshot](screenshots/ld20x6-screenshot.png)


Licensing
---------
Code is licensed under the [MIT license][]; see the `LICENSE` file for details.
Content is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported][by-nc-sa],
with the following exception:

Fonts are from the Grixel Kyrou series by Nikos Giannakopoulos, fetched from dafont.com, which are licensed under the
[Creative Commons Attribution-NoDerivs 2.5][by-nd] license:

* [Grixel Kyrou 5 wide](http://www.dafont.com/grixel-kyrou-5-wide.font)
* [Grixel Kyrou 7 wide](http://www.dafont.com/grixel-kyrou-7-wide.font)
* [Grixel Kyrou 9](http://www.dafont.com/grixel-kyrou-9.font)

[MIT license]: http://www.opensource.org/licenses/mit-license
[by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/3.0/
[by-nd]: http://creativecommons.org/licenses/by-nd/2.5/

Also, the packages in `compat` each have their own licenses:

* [ordereddict](http://pypi.python.org/pypi/ordereddict/): [MIT license][]


Prerequisites
-------------
You'll need to [install `pipenv`][]. If you already have Python and `pip` installed, the simplest way to install
`pipenv` is:
```sh
pip install --user pipenv
```

[install `pipenv`]: https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv


Setup
-----
Create a virtual environment and install the needed dependencies using `pipenv`:
```sh
pipenv install
```


Running
-------
Run `ld20.py` with `pipenv`:
```sh
pipenv run python ld20.py
```


Creation
--------
* All code was written in [Python][] using [PyGame][]. Editing was performed using [Vim][].
* Sound effects were generated using [sfxr][] with tweaks done in [Audacity][].
* Music was created using [SunVox][].
* Graphics were created by hand using [the GIMP][] and [Inkscape][].
* Code has been formatted using [black][].

[Python]: http://python.org/
[PyGame]: http://pygame.org/
[Vim]: http://www.vim.org/
[sfxr]: http://www.drpetter.se/project_sfxr.html
[Audacity]: http://audacity.sourceforge.net/
[SunVox]: https://warmplace.ru/soft/sunvox/
[the GIMP]: http://www.gimp.org/
[Inkscape]: http://inkscape.org/
[black]: https://black.readthedocs.io/en/stable/
